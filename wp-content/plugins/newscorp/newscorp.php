<?php
/**
 * This is main configuration file of plugins
 */

/*
Plugin Name: News Corp Test
Plugin URI: https://sushilwp.wordpress.com/
Description: This plugin is used to demostrate Technical ability to News Corp
Version: 1.0.0
Author: Sushil Adhikari
Author URI: https://sushilwp.wordpress.com/
License: GPLv2 or later
Text Domain: newscorp
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define NC_PLUGIN_FILE.
if ( ! defined( 'NC_PLUGIN_FILE' ) ) {
	define( 'NC_PLUGIN_FILE', __FILE__ );
}

// Include the main newcorp class.
if ( ! class_exists( 'NewsCorp' ) ) {
	include_once dirname( __FILE__ ) . '/includes/class-newscorp.php';
}
//create instance of woocommerce
return new NewsCorp();