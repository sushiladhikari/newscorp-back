<?php
/**
* This file includes all the functional aspects of this function
* @author Sushil Adhikari
* @version 1.0
*/

$generate_random_hand = new GenerateRandomHand();

//create card deck 
$is_valid_entry = $generate_random_hand->validate_entry();
if( false === $is_valid_entry ) {
	echo $generate_random_hand->get_error_message();
	return false;
}
$create_card_deck = $generate_random_hand->create_card_deck();
$error = $generate_random_hand->get_error_message();
$generate_card = $generate_random_hand->get_deck_of_card();

if( !empty( $error ) ) {
	echo $generate_random_hand->get_error_message();
	return false;
}
$card_on_team_hand = $generate_random_hand->destribute_deck_on_team_players();