<?php
/**
* News corp setup
*/
if( !defined( 'ABSPATH' ) ) {
	exit();
}

class NewsCorp {
	
	/**
	 * Newcorporation Constructor.
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
	}


	/**
	 * Define WC Constants.
	 */
	private function define_constants() {
		//defines abspath of the plugins
		define( 'NC_ABSPATH', dirname( NC_PLUGIN_FILE ) . '/' );
	}

	/**
	 * Include required core files 
	 */
	public function includes() {
		//add card generation
		require_once NC_ABSPATH . 'includes/class-generate-random-hand.php';
		//functional file
		require_once NC_ABSPATH . 'functions.php';
	}

}